﻿using AspNetCore.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mongo.RepositoryPattern.Repository
{
    public interface IRepository<T> where T : IMongoEntity
    {
        Task<T> Save(T entity);
        Task<T> Details(Expression<Func<T, bool>> predicate);
        Task<bool> Delete(T entity);
        Task<bool> DeleteRange(Expression<Func<T, bool>> predicate);
        Task<bool> IsExist(string id);
        Task<List<T>> GetAll();
        Task<List<T>> GetAll(Expression<Func<T, bool>> expression);
        Task<bool> Update(T entity, List<object> avoidedProperties);
        Task<bool> Update(T entity);

    }
}
