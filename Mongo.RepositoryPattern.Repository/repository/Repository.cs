﻿using AspNetCore.MongoDB;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Mongo.RepositoryPattern.Repository.repository
{
    public class Repository<T> : IRepository<T> where T : IMongoEntity
    {
        private readonly IMongoOperation<T> _mongoRepository;

        public Repository
            (
                IMongoOperation<T> mongoRepository
            )
        {
            _mongoRepository = mongoRepository;
        }


        public Task<bool> Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteRange(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<T> Details(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetAll()
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetAll(Expression<Func<T, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> IsExist(string id)
        {
            var entity = await _mongoRepository.GetByIdAsync(id);

            return entity == null ? false : true;
        }

        public async Task<T> Save(T entity)
        {
            return await _mongoRepository.SaveAsync(entity);
        }

        public async Task<bool> Update(T entity, List<object> avoidedProperties)
        {
            var isExist = await IsExist(entity.Id);

            if(!isExist)
            {
                throw new Exception("Data not found");
            }

            var filter = Builders<T>.Filter.Eq(t => t.Id, entity.Id);
            var update = GetUpdateDefination(avoidedProperties, entity);

            var upsert = new FindOneAndUpdateOptions<T>()
            {
                IsUpsert = false
            };

            try
            {
                var response = await _mongoRepository.FindOneAndUpdateAsync(filter, update, upsert);

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool MatchProperty(List<object> avoidedProperties, string entityProperty)
        {
            foreach (var item in avoidedProperties)
            {
                foreach (PropertyInfo p in item.GetType().GetProperties())
                {
                    if(p.Name == entityProperty)
                    {
                        return true;
                    }
                }
                    
            }
            return false;
        }

        private PropertyInfo[] GetPropertyInfo(T entity)
        {
            return entity.GetType().GetProperties();
        }

        private UpdateDefinition<T> GetUpdateDefination(List<object> avoidedProperties, T entity)
        {
            List<UpdateDefinition<T>> listUpdate = new List<UpdateDefinition<T>>();

            foreach(var property in GetPropertyInfo(entity))
            {
                if(!MatchProperty(avoidedProperties,property.Name))
                {
                    listUpdate.Add(Builders<T>.Update.Set(property.Name, property.GetValue(entity)));
                }
            }

            return Builders<T>.Update.Combine(listUpdate);
            
        }

        public async Task<bool> Update(T entity)
        {
            var isExist = await IsExist(entity.Id);

            if (!isExist)
            {
                throw new Exception("Data not found");
            }

            var response = _mongoRepository.UpdateAsync(entity.Id, entity);

            if (response.IsCompleted)
            {
                return true;
            }

            return false;
        }
    }
}
